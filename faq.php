<?php
    require_once('jp/jp.inc.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">
        <meta name="robots" content="noindex, nofollow">
        <meta name="author" content="idevelopment.pl">        
        <meta name="description" content="" >        
        <meta name="keywords" content="" >                                

        <!-- og -->
        <meta property="og:title" content="" >
        <meta property="og:type" content="website" >                
        <meta property="og:url" content="" >
        <meta property="og:image" content="" >    
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="200">
        <meta property="og:image:height" content="200">        
        <meta property="og:site_name" content="" >        
        <meta property="og:description" content="" >                           

        <!-- styles -->
        <title>Negocjatornia</title>
        <link rel="shortcut icon" type="image/png" href="img/Favicon_16x16.png">                
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">           


        <!-- old browsers support-->        
        <!--[if lt IE 9]>            
            <script src="js/html5shiv.min.js"></script>
            <script src="js/respond.min.js"></script>                                     
        <![endif]-->                
        <!--[if IE 9 ]>
            <script src="js/placeholders.min.js"></script>                
        <![endif]-->

        <!-- JS disabled -->
        <!--noscript><link href="css/jsDisabled.css" rel="stylesheet"></noscript-->

    </head>
    <body class="index index-faq" id="page-top">
        <header>
            <nav class="menu container">
                <div class='row'>
                    <div class="col-xs-24 menu-new">
                        <img src="img/logotyp_standard.png" alt=""/>
                        <ul>
                            <li><a href="index.php" class="page-scroll">O FIRMIE</a></li>
                            <li><a href="kredyty.php">KREDYTY</a></li>
                            <li><a href="negocjacje.php">NEGOCJACJE</a></li>
                            <li class="active"><a href="#page-top">FAQ</a></li>
                            <li><a href="#contact" class="page-scroll">KONTAKT</a></li>
                        </ul>
                    </div>                    
                    <div class="hamburger">
                        <div class="roll">
                            <div class="beef"></div>
                        </div>
                        <!-- om om om! -->
                    </div>                
                </div>
            </nav>
        </header>

        <section class="faq">
            <div class="container">
                <div class="row">
                    <div class="col-xs-24">
                        <h1 class="jp-edit jp-elem" id="faq-h1">{{faq-h1}}</h1>                                                
                        <h2 class="faq-title jp-edit jp-elem" id="faq-1-h2">{{faq-1-h2}}</h2>
                    </div>                    

                    <div class="col-xs-24 accordion">
                        <div class="accordion-items items">
                            <div class="item jp-edit">
                                <h2 class="jp-elem opened" id="faq-1-1-h">{{faq-1-1-h}}</h2>
                                <div class="accordion-child" style="display: block;">
                                    <p class="jp-elem" id="faq-1-1-p">{{faq-1-1-p}}</p>
                                </div>
                            </div>
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-1-2-h">{{faq-1-2-h}}</h2>
                                <div class="accordion-child">
                                    <ul>
                                        <li class="jp-elem" id="faq-1-2-p-0">{{faq-1-2-p-0}}</li>
                                        <li class="jp-elem" id="faq-1-2-p-1">{{faq-1-2-p-1}}</li>
                                        <li class="jp-elem" id="faq-1-2-p-2">{{faq-1-2-p-2}}</li>
                                        <li class="jp-elem" id="faq-1-2-p-3">{{faq-1-2-p-3}}</li>
                                        <li class="jp-elem" id="faq-1-2-p-4">{{faq-1-2-p-4}}</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-1-3-h">{{faq-1-3-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-1-3-p">{{faq-1-3-p}}</p>
                                </div>
                            </div>
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-1-4-h">{{faq-1-4-h}}</h2>
                                <div class="accordion-child">
                                    <ul>
                                        <li class="jp-elem" id="faq-1-4-p-0">{{faq-1-4-p-0}}</li>
                                        <li class="jp-elem" id="faq-1-4-p-1">{{faq-1-4-p-1}}</li>
                                        <li class="jp-elem" id="faq-1-4-p-2">{{faq-1-4-p-2}}</li>
                                        <li class="jp-elem" id="faq-1-4-p-3">{{faq-1-4-p-3}}</li>
                                        <li class="jp-elem" id="faq-1-4-p-4">{{faq-1-4-p-4}}</li>
                                    </ul>
                                </div>
                            </div>                            
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-1-5-h">{{faq-1-5-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-1-5-p">{{faq-1-5-p}}</p>
                                </div>   
                            </div>                        
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-1-6-h">{{faq-1-6-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-1-6-p">{{faq-1-6-p}}</p>
                                </div>                            
                            </div>                        
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-1-7-h">{{faq-1-7-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-1-7-p">{{faq-1-7-p}}</p>
                                </div>
                            </div>      
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-1-8-h">{{faq-1-8-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-1-8-p">{{faq-1-8-p}}</p>
                                </div>                            
                            </div>                                                    
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-1-9-h">{{faq-1-9-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-1-9-p">{{faq-1-9-p}}</p>
                                </div>                        
                            </div>                                                    
                        </div>
                    </div>
                    <div class="col-xs-24">
                        <h2 class="faq-title jp-edit jp-elem" id="faq-2-h2">{{faq-2-h2}}</h2>
                    </div>

                    <div class="col-xs-24 accordion">
                        <div class="accordion-items items">
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-2-1-h">{{faq-2-1-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-2-1-p">{{faq-2-1-p}}</p>
                                </div>                            
                            </div>
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-2-2-h">{{faq-2-2-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-2-2-p">{{faq-2-2-p}}</p>
                                </div>                            
                            </div>
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-2-3-h">{{faq-2-3-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-2-3-p">{{faq-2-3-p}}</p>
                                </div>                            
                            </div>
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-2-4-h">{{faq-2-4-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-2-4-p">{{faq-2-4-p}}</p>
                                </div>                            
                            </div>
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-2-5-h">{{faq-2-5-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-2-5-p">{{faq-2-5-p}}</p>
                                </div>                            
                            </div>
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-2-6-h">{{faq-2-6-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-2-6-p">{{faq-2-6-p}}</p>
                                </div>                            
                            </div>
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-2-7-h">{{faq-2-7-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-2-7-p">{{faq-2-7-p}}</p>
                                </div>                            
                            </div>
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-2-8-h">{{faq-2-8-h}}</h2>
                                <div class="accordion-child jp-elem" id="faq-2-8-p">
{{faq-2-8-p}}
                                </div>                            
                            </div>
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-2-9-h">{{faq-2-9-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-2-9-p">{{faq-2-9-p}}</p>
                                </div>                            
                            </div>
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-2-10-h">{{faq-2-10-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-2-10-p">{{faq-2-10-p}}</p>
                                </div>                            
                            </div>                        
                        </div>
                    </div>
                    <div class="col-xs-24">
                        <h2 class="faq-title jp-edit jp-elem" id="faq-3-h2">{{faq-3-h2}}</h2>
                    </div>

                    <div class="col-xs-24 accordion">
                        <div class="accordion-items items">
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-3-1-h">{{faq-3-1-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-3-1-p">{{faq-3-1-p}}</p>
                                </div>
                            </div>                     
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-3-2-h">{{faq-3-2-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-3-2-p">{{faq-3-2-p}}</p>
                                </div>
                            </div>                     
                            <div class="item jp-edit">
                                <h2 class="jp-elem" id="faq-3-3-h">{{faq-3-3-h}}</h2>
                                <div class="accordion-child">
                                    <p class="jp-elem" id="faq-3-3-p">{{faq-3-3-p}}</p>
                                </div>
                            </div>                     
                        </div>  
                    </div>             
                </div>
        </section>

        <section class="contact" id="contact">
            <div class="contact-fill">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 jp-edit">
                        <h1 class="jp-elem" id="index-footer-h1">{{index-footer-h1}}</h1>
                        <div class="contact-item phone">
                            <img src="img/telefon_kontakt.png" alt=""/>
                            <div>
                                <p><a href="{{index-footer-tel1-href}}" class="jp-elem" id="index-footer-tel1">{{index-footer-tel1-text}}</a></p>
                                <p><a href="{{index-footer-tel1-href}}" class="jp-elem" id="index-footer-tel2">{{index-footer-tel2-text}}</a></p>                                
                            </div>
                        </div>
                        <div class="contact-item mail">
                            <img src="img/kopertka_mail_kontakt.png" alt=""/>
                            <div>
                                <p><a href="{{index-footer-email-href}}" class="jp-elem" id="index-footer-email">{{index-footer-email-text}}</a></p>                                                             
                            </div>
                        </div>
                        <div class="contact-item address">
                            <img src="img/lokalizacja_kontakt.png" alt=""/>
                            <div>
                                <p class="jp-elem" id="index-footer-addr1">{{index-footer-addr1}}</p>                                                             
                                <p class="jp-elem" id="index-footer-addr2">{{index-footer-addr2}}</p>                                                             
                            </div>
                        </div>
                    </div>
                    <form class="col-sm-12 col-lg-8 col-lg-offset-4 hidden-xs contact-form" id="contact-form" role="form" action="" method="post">
                        <input type="hidden" name="form" value="kontakt"/>
                        <h1>Napisz do nas</h1>
                        <p><input type="text" name="nazwisko" placeholder="Imię i nazwisko"/></p>
                        <p><input type="text" name="telefon" placeholder="Numer telefonu"/></p>
                        <p><input type="text" name="email" placeholder="E-mail"/></p>
                        <p><input type="submit" value="wyślij"/></p>
                        <!--input name="warunki" class="first" type="checkbox" value="tak"/> Akceptuję warunki
                        <input name="zgoda" type="checkbox" value="tak"/> Wyrażam zgodę-->
                        <input name="regulamin" type="checkbox" value="tak"/> Zapoznałem się z <a href="attch/regulamin-strony.pdf">regulaminem</a> / akceptuję <a href="attch/regulamin-strony.pdf">regulamin</a>
                    </form>
                </div>
            </div>
            </div>            
        </section>
        <div class="map" id="map-canvas"></div>        

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul>
                            <li><a href="#">2015 Negocjatornia</a></li>
                            <li><a href="attch/polityka-prywatnosci.pdf" target="_blank">Polityka prywatności</a></li>
                            <li><a href="attch/regulamin-strony.pdf" target="_blank">Regulamin</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-12">
                        <ul class="pull-right">
                            <li>wykonanie:</li>
                            <li><a href="http://www.960pikseli.pl">www.960pikseli.pl</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </footer>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDkR1lw4SMGMFquqdryC6eBE-88HpNvcM"></script>        
        <script src="js/jquery.min.js"></script>        
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/velocity.min.js"></script>
        <script src="js/carousel.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/messages_pl.min.js"></script> 
        <script src="js/eventSpy.js"></script>
        <script src="js/player.js"></script>
<?php jp_edit(); ?>
    </body>
</html>