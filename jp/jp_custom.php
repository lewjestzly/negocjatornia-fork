<?php
define('__GRID',24);

if($_POST)
{
	$return ='';
	if(isset($_POST['create']))
	{
		unset($_POST['create']);
		foreach($_POST['data'] as $i => $content)
		{
			if(@file_put_contents('data/'.$content.'.tpl',$_POST['val'][$i]))
			{
				$return .= $content.': '.$_POST['val'][$i]."\n";
			}
			else
			{
				$return .= $content.': error'."\n";
			}
		}
	}
	else if(isset($_POST['remove']))
	{
		unset($_POST['remove']);
		foreach($_POST['data'] as $content)
		{
			unlink('data/'.$content.'.tpl');
		}
	}
	echo $return;
}
function slides($page='index',$subt=true)
{
	$counter = count(glob('jp/data/slide-'.$page.'-*-title*'));
	$return = '';
	for($i=0;$i<$counter;$i++)
	{
		$img = file_get_contents("jp/data/slide-$page-$i-img.tpl");
		$title = file_get_contents("jp/data/slide-$page-$i-title.tpl");
		$return .= <<<EOL
        <div class="slide jp-edit" id="slide-$i" style="background-image:url('$img');">
            <div class="slide-title">
                <p class="jp-elem" id="slide-$page-$i-title">$title</p>
                <!--img src="img/ksztalt_pod_tekst_top.png" alt=""/-->
            </div>
EOL;
		if($subt)
		{
			$stitle = file_get_contents("jp/data/slide-$page-$i-stitle.tpl");
			$return .= <<<EOL

            <div class="slide-subtitle">
                <p class="jp-elem" id="slide-$page-$i-stitle">$stitle</p>
                <!--img src="img/ksztalt_pod_tekst_top.png" alt=""/-->
            </div>
EOL;
		}
		$return .= <<<EOL

        </div>
EOL;
	}
	if($counter>1)
	{
		$counter--;
		$return .= <<<EOL
        <div class="slide-nav slide-left" onclick="slideLeft()"><img src='img/idev/left-arrow.png' alt=''/></div>
        <div class="slide-nav slide-right" onclick="slideRight($counter)"><img src='img/idev/right-arrow.png' alt=''/></div>

EOL;
	}
	return $return;

}
function partners()
{
	$counter = count(glob('jp/data/index-partners-img-*'));
	$return = '';
	for($i=0;$i<$counter;$i++)
	{
		$img = file_get_contents("jp/data/index-partners-img-$i.tpl");
		$return .= <<<EOL
					--><div class="col-md-4 col-sm-8 col-xs-24"><img class="jp-elem" id="index-partners-img-$i" src="$img" alt=""></div><!--

EOL;
	}
	return $return;

}
?>