<?php
session_start();

if(file_exists('jp/jp_custom.php'))
	require_once 'jp/jp_custom.php';

if(!isset($_SESSION['jp_edit']))
{
	$_SESSION['jp_edit'] = false;
}

$jp_ready = $_SESSION['jp_edit'];

function jp_edit()
{
	global $jp_ready;
	$script=<<<EOL
		<script src="jp/jquery.bpopup.min.js"></script>
		<script src="jp/jquery.ui.widget.js"></script>
		<script src="jp/jquery.iframe-transport.js"></script>
		<script src="jp/jquery.fileupload.js"></script>
		<script src="jp/jp_custom.js"></script>
		<script src="jp/jp.js"></script>
EOL;
	if($jp_ready)
		print $script;
	ob_end_flush();
}

function jp($buffer) {
    $buffer = preg_replace_callback("/\{\{(.*?)\}\}/", function($matches) {
    	global $jp_ready;
    	$bfr_tmp = '';
    	$file = 'jp/data/'.$matches[1].'.tpl';
				    if(file_exists($file))
				    {
				    	if (strpos($matches[1],'jp') !== false)
				    	{
				    		if($jp_ready)
				    			$bfr_tmp .= '<ins id="'.$matches[1].'" class="jp-elem">';
				    		$bfr_tmp .=(file_get_contents($file));
				    		if($jp_ready)
				    			$bfr_tmp .= '</ins>';
				    		return $bfr_tmp;
				    	}
			    		return (file_get_contents($file));
				    }
				    else
				    {
				    	if (strpos($matches[1],'jp') !== false)
				    	{
				    		if($jp_ready)
				    			$bfr_tmp .= '<ins id="'.$matches[1].'" class="jp-elem">';
				    		$bfr_tmp .= "[".$matches[1]."]";
				    		if($jp_ready)
				    			$bfr_tmp .= '</ins>';
				    		return $bfr_tmp;
				    	}
				    	return "[".$matches[1]."]";
				    }
				}, $buffer);
    return $buffer;
}

ob_start("jp");
?>