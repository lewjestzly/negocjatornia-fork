$(document).ready(function(){
	// SLIDES
	var $slides = $('.intro-carousel');
	if($slides.length)
	{
		$slides.before('<div class="jp-add-btns"><span class="jp-btn jp-btn-add" id="add-slide">+</span><span class="jp-btn jp-btn-del" id="del-slide">-</span></div>');
		$.each($slides.children('div.slide'),function(i,e){
			var bg_url = $(e).css('background-image').replace(/.*\s?url\([\'\"]/, '').replace(/[\'\"]\).*/, '');
			console.log(bg_url);
			var name = /(.*)?-(\d.*)-/g.exec($(e).find('.slide-title p').attr('id'))[0];
			console.log('cr img '+name);
			$('<div style="display:none"></div>').appendTo($(e)).append('<img src="'+bg_url+'" class="jp-elem" id="'+name+'img">');
		});
			var $slide_last = $(".intro-carousel > div.slide").last();
			var slide_last_id = $slide_last.find('.slide-title p').attr('id');
			var slide_num = parseInt( slide_last_id.match(/\d+/g), 10 );
			var slide_name = /slide-(.*)?-\d.*-/g.exec(slide_last_id)[1];
		$('#add-slide').bind('click',function()
		{
			console.log(slide_name + ' '+slide_num);
			$.post('jp/jp_custom.php',{
				create: true,
				data: ['slide-'+slide_name+'-'+(slide_num+1)+'-title','slide-'+slide_name+'-'+(slide_num+1)+'-stitle','slide-'+slide_name+'-'+(slide_num+1)+'-img'],
				val: [$slide_last.find('.slide-title p').html(), $slide_last.find('.slide-subtitle p').html(), $slide_last.css('background-image').replace(/.*\s?url\([\'\"]/, '').replace(/[\'\"]\).*/, '')]
			})
			.done(function(d){
				console.log('done '+d);
				location.reload();
			})
			.fail(function(d){
				console.log('nope '+d);
			});
		});
		$('#del-slide').bind('click',function(){
			if(slide_num==0)
				return;
			$.post('jp/jp_custom.php',{
				remove: true,
				data: ['slide-'+slide_name+'-'+slide_num+'-title','slide-'+slide_name+'-'+slide_num+'-stitle','slide-'+slide_name+'-'+slide_num+'-img']
			})
			.done(function(d){
				console.log('done '+d);
				location.reload();
			})
			.fail(function(d){
				console.log('nope '+d);
			});
		});
	}


	var $partners = $('.partners-list');
	if($partners.length)
	{
	$partners.before('<div class="jp-add-btns"><span class="jp-btn jp-btn-add" id="add-partner">+</span><span class="jp-btn jp-btn-del" id="del-partner">-</span></div>');
			var $partner_last =$partners.find('img').last();
			var partner_last_id = $partner_last.attr('id');
			var partner_num = parseInt( partner_last_id.match(/\d+/g), 10 );
			var partner_src = $partner_last.attr('src');
		$('#add-partner').bind('click',function()
		{
			$.post('jp/jp_custom.php',{
				create: true,
				data: ['index-partners-img-'+(partner_num+1)],
				val: [partner_src]
			})
			.done(function(d){
				console.log('done '+d);
				location.reload();
			})
			.fail(function(d){
				console.log('nope '+d);
			});
		});
		$('#del-partner').bind('click',function(){
			if(partner_num==0)
				return;
			$.post('jp/jp_custom.php',{
				remove: true,
				data: ['index-partners-img-'+(partner_num)],
			})
			.done(function(d){
				console.log('done '+d);
				location.reload();
			})
			.fail(function(d){
				console.log('nope '+d);
			});
		});
	}
	
});