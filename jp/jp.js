$(document).ready(function(){


	$('head').append('<link rel="stylesheet" href="jp/jp.css">');
'use strict';
	$('<span/>').attr('id','jp-logout').addClass('jp-btn').html('zakończ').appendTo($('body')).bind('click',function()
		{
			location.href = 'admin/?logout';
		});


	$.each($('.jp-edit'),function(i,e){

		var $elem = $(e).find('.jp-elem');
		console.log('searching '+$(e).attr('id')+': '+$elem.length);
		if($elem.length)
			$.each($elem,function(i,e){
				if(!$.trim($(e).text()))
					$(e).text('{{puste}}: ['+$(e).attr('id')+']');
			});
		else
		{
			if(!$.trim($(e).text()))
				$(e).text('{{puste}}: ['+$(e).attr('id')+']');
			$elem = $(this);
			$elem.addClass('jp-elem');
		}
		$(e).addClass('jp-click');

		if(($elem.parent()).is('a'))
		{
			($elem.parent()).bind('click',function(ev)
				{
					ev.preventDefault();
				});
		}


		$(e).bind('click', function(ev) {

            ev.preventDefault();
            var strip = false;
            var $popup = $('<div class="jp-popup"></div>');
            $('<del class="jp-popup-close">x</del>').appendTo($popup);
            var $popup_orig = $popup;

            // var add = false;



			$.each($elem,function(j,f){

				// var $parents = $(f).parents('.jp-add');

				// if($parents.length)
				// {
				// 	if(!add)
				// 	{
				// 		var $jp_add = $('<div class="jp-popup-add"></div>');
				// 		$jp_add.appendTo($popup);
				// 		$jp_add.prepend('<i class="jp-btn-add">dodaj</i><i class="jp-btn-del">usuń</i>');
				// 		$popup = $jp_add;
				// 	}
				// 	add = true;
				// 	console.log('jp-add: '+$(f).parents('.jp-add').attr('class'));
				// }
				// else
				// 	add = false;

				var id = $(f).attr('id');



                $popup.append('<span>'+id+'</span>');

                if($(f).is('img'))
                {
                	var $img_orig = $(f).clone();
                	var src = '';
                	var $field = $('<div class="jp-field"></div>');
                	$popup.append($field);
                	$field.append($img_orig);
                	$field.append('<br><input type="file" class="'+id+'" name="files[]">');
                	if($img_orig.hasClass('jp-bgimg'))
                	{
                		$("input[type='file']").addClass('file-bg');
                		src = ($img_orig).css('background-image');
                		src = src.replace(/.*\s?url\([\'\"]/, '').replace(/[\'\"]\).*/, '');
                	}
                	else
                	{
	                	src = $img_orig.attr('src');
                	}
	                var $input = $('<input class="jp-content '+id+'" type="text" id="jp-'+id+'" value="'+src+'">');
            		$field.append($input);
                }
                else if($(f).is('a'))
                {
	                var content = $(f).html();
	                var href = $(f).attr('href');
	                var $input = $('<textarea class="jp-content" id="jp-'+id+'-text">'+content+'</textarea>');
	                var $input_href = $('<input class="jp-content" id="jp-'+id+'-href" value="'+href+'">');

            		$popup.append($input);
            		$popup.append($input_href);
                }
                else
                {
                	
	                var content = $(f).html().replace(/<br\s*[\/]?>/gi, "");
	                console.log(content);
	                var $textarea = $('<textarea class="jp-content" id="jp-'+id+'">'+content+'</textarea>');

                	$popup.append($textarea);
                }
                $popup_orig.find('textarea, input').css('font-family',$('body').css('font-family'));

                // if($(f).hasClass('jp-nohtml'))
                	strip = true;

			})

	     //    var $last = $('.jp-popup .jp-popup-add input[type="file"]:last'); //only input file-type
		    // var num = 1;
		    // if($last.length)
		    //     num = parseInt( $last.prop('id').match(/\d+/g), 10 ) + 1;
		    // console.log(num);
		    
			var $button = $('<button class="jp-save">Zapisz</button>');

			$popup_orig.append($button);

            $popup_orig.modal({
				opacity:95,
				overlayClose:false,
				closeClass:'jp-popup-close'
				});
				var fail = false;

				var $popup_reload = $('<div class="jp-popup"><span>Zmiany zapisano</span></div>');
				var $rld_button = $('<button class="jp-save jp-reload">Odśwież aby zobaczyć</button>');
				$rld_button.appendTo($popup_reload);
			$button.bind('click',function(){
				var info_log = '';
				$inputs = $popup_orig.find('input[id], textarea[id]');
				var count = $inputs.length;
				var ind = 0;

				var ids = [];
				var texts = [];
				$.each($inputs,function(i,e){
					var data = $(e).attr('id').replace('jp-','');
					var value = $(e).val();
					if(!value || (/\{\{puste\}\}:/i.test(value)))
						value = " ";
						ids.push(data);
						texts.push(value);
				});
					$.ajax({
						method: 'POST',
						url: 'jp/jp.php',
						data: {
								id: ids,
								text: texts,
								strips: strip
								}
					})
					.done(function(d){
						console.log('done '+d);
						ind++;
					})
					.fail(function(d){
						console.log('fail '+d);
						fail = true;
					});
				$.modal.close();
	            $popup_reload.modal({
					opacity:95,
					overlayClose:false,
					onOpen: function (dialog) {
							dialog.overlay.fadeIn('fast', function () {
								dialog.container.slideDown('fast', function () {
									dialog.data.fadeIn('slow');
								});
							});
						}
					});
			});
			$rld_button.bind('click',function(){
				// console.log('siema');
				location.reload();
			});


		});

	});

});
$(document).on("click", "input[type='file']", function () {
	var i_img = $(this).attr('class');
    var url = 'jp/upload/';
    var bg_img = $(this).hasClass('file-bg');
	console.log(i_img);
    $(this)
            .fileupload({
		        url: url,
		        dataType: 'json',
		        done: function (e, data) {
		            $.each(data.result.files, function (index, file) {
		            	var new_src = url+'files/'+file.name;
		                $('#jp-'+i_img).val(new_src);
		                var $img_replace = $('.jp-popup').find('#'+i_img);

		                if(bg_img)
		                	$img_replace.css('background-image','url('+new_src+')');
		                else
		                	$img_replace.attr('src',new_src).addClass('img');
		            });
		        }
            });
});