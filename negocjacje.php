<?php
    require_once('jp/jp.inc.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">
        <meta name="robots" content="noindex, nofollow">
        <meta name="author" content="idevelopment.pl">        
        <meta name="description" content="" >        
        <meta name="keywords" content="" >                                

        <!-- og -->
        <meta property="og:title" content="" >
        <meta property="og:type" content="website" >                
        <meta property="og:url" content="" >
        <meta property="og:image" content="" >    
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="200">
        <meta property="og:image:height" content="200">        
        <meta property="og:site_name" content="" >        
        <meta property="og:description" content="" >                           

        <!-- styles -->
        <title>Negocjatornia</title>
        <link rel="shortcut icon" type="image/png" href="img/Favicon_16x16.png">                
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">           

  
        <!-- old browsers support-->        
        <!--[if lt IE 9]>            
            <script src="js/html5shiv.min.js"></script>
            <script src="js/respond.min.js"></script>                                     
        <![endif]-->                
        <!--[if IE 9 ]>
            <script src="js/placeholders.min.js"></script>                
        <![endif]-->

        <!-- JS disabled -->
        <!--noscript><link href="css/jsDisabled.css" rel="stylesheet"></noscript-->

    </head>
    <body class="negotiations" id="page-top">
        <header>
            <nav class="menu container">
                <div class='row'>
                    <div class="col-xs-24 menu-new">
                        <img src="img/logotyp_standard.png" alt=""/>
                        <ul>
                            <li><a href="index.php">O FIRMIE</a></li>
                            <li><a href="kredyty.php">KREDYTY</a></li>
                            <li class="active"><a href="#page-top" class="page-scroll">NEGOCJACJE</a></li>
                            <li><a href="faq.php">FAQ</a></li>
                            <li><a href="#contact" class="page-scroll">KONTAKT</a></li>
                        </ul>
                    </div>                                       
                    <div class="hamburger">
                        <div class="roll">
                            <div class="beef"></div>
                        </div>
                        <!-- om om om! -->
                    </div>                
                </div>
            </nav>
        </header>

        <section class="intro-carousel-update">
            <div class="slide slide-update">
                <div class="container">
                    <div class="row slide-box">
                        <div class="col-md-8">
                            <div class="slide-box-item jp-edit">
                                <h1 class="title"><strong class="jp-elem" id="negocjacje-box-0-h1">{{negocjacje-box-0-h1}}</strong> <span class="jp-elem" id="negocjacje-box-0-h1-span">{{negocjacje-box-0-h1-span}}</span></h1>
                                <img class="arrow-left" src="img/update-20-09/strzalki_top.png" alt=""/>
                                <p class="subtitle">                                    
                                    <span class="jp-elem" id="negocjacje-box-0-sub-0-span">{{negocjacje-box-0-sub-0-span}}</span> <strong class="jp-elem" id="negocjacje-box-0-sub-0">{{negocjacje-box-0-sub-0}}</strong><br>
                                    <span class="jp-elem" id="negocjacje-box-0-sub-1-span">{{negocjacje-box-0-sub-1-span}}</span> <strong class="jp-elem" id="negocjacje-box-0-sub-1">{{negocjacje-box-0-sub-1}}</strong>
                                </p>
                                <div class="slide-box-footer">
                                    <p class="jp-elem" id="negocjacje-box-0-foot">{{negocjacje-box-0-foot}}</p>
                                    <p class="big-value jp-elem" id="negocjacje-box-0-val">{{negocjacje-box-0-val}}</p>
                                    <img src="img/update-20-09/1_.png" alt=""/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="slide-box-item jp-edit">
                                <h1 class="title"><strong class="jp-elem" id="negocjacje-box-1-h1">{{negocjacje-box-1-h1}}</strong> <span class="jp-elem" id="negocjacje-box-1-h1-span">{{negocjacje-box-1-h1-span}}</span></h1>
                                <img class="arrow-left" src="img/update-20-09/strzalki_top.png" alt=""/>
                                <p class="subtitle">                                    
                                    <span class="jp-elem" id="negocjacje-box-1-sub-0-span">{{negocjacje-box-1-sub-0-span}}</span> <strong class="jp-elem" id="negocjacje-box-1-sub-0">{{negocjacje-box-1-sub-0}}</strong><br>
                                    <span class="jp-elem" id="negocjacje-box-1-sub-1-span">{{negocjacje-box-1-sub-1-span}}</span> <strong class="jp-elem" id="negocjacje-box-1-sub-1">{{negocjacje-box-1-sub-1}}</strong>
                                </p>
                                <div class="slide-box-footer">
                                    <p class="jp-elem" id="negocjacje-box-1-foot">{{negocjacje-box-1-foot}}</p>
                                    <p class="big-value jp-elem" id="negocjacje-box-1-val">{{negocjacje-box-1-val}}</p>
                                    <img src="img/update-20-09/1_.png" alt=""/>
                                </div>                                
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="slide-box-item jp-edit">
                                <h1 class="title"><strong class="jp-elem" id="negocjacje-box-2-h1">{{negocjacje-box-2-h1}}</strong> <span class="jp-elem" id="negocjacje-box-2-h1-span">{{negocjacje-box-2-h1-span}}</span></h1>
                                <img class="arrow-left" src="img/update-20-09/strzalki_top.png" alt=""/>
                                <p class="subtitle">                                    
                                    <span class="jp-elem" id="negocjacje-box-2-sub-0-span">{{negocjacje-box-2-sub-0-span}}</span> <strong class="jp-elem" id="negocjacje-box-2-sub-0">{{negocjacje-box-2-sub-0}}</strong><br>
                                    <span class="jp-elem" id="negocjacje-box-2-sub-1-span">{{negocjacje-box-2-sub-1-span}}</span> <strong class="jp-elem" id="negocjacje-box-2-sub-1">{{negocjacje-box-2-sub-1}}</strong>
                                </p>
                                <div class="slide-box-footer">
                                    <p class="jp-elem" id="negocjacje-box-2-foot">{{negocjacje-box-2-foot}}</p>
                                    <p class="big-value jp-elem" id="negocjacje-box-2-val">{{negocjacje-box-2-val}}</p>
                                    <img src="img/update-20-09/1_.png" alt=""/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>      
        <section class="check-offer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-24 jp-edit">
                        <h1 class="jp-elem" id="negocjacje-about-h1">{{negocjacje-about-h1}}</h1>
                        <h2 class="jp-elem" id="negocjacje-about-h2">{{negocjacje-about-h2}}</h2>
                        <h2 class="jp-elem" id="negocjacje-about-h2-2">{{negocjacje-about-h2-2}}</h2>
                    </div>
                </div>    
                <div class="row items">
                    <div class="col-lg-9 col-lg-offset-3 col-md-9 col-md-offset-3 col-sm-12 col-xs-24 item">
                        <div class="col-md-5 col-sm-6 col-xs-24"><img src="img/zaoszczedz-pieniadze.png" alt=""/></div>
                        <div class="col-md-19 col-sm-18 col-xs-24 jp-edit">
                            <h3 class="jp-elem" id="negocjacje-about-1-h">{{negocjacje-about-1-h}}</h3>
                            <p class="jp-elem" id="negocjacje-about-1-p">{{negocjacje-about-1-p}}</p>                           
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-24 item">
                        <div class="col-md-5 col-sm-6 col-xs-24"><img src="img/mdm.png" alt=""/></div>
                        <div class="col-md-19 col-sm-18 col-xs-24 jp-edit">                                        
                            <h3 class="jp-elem" id="negocjacje-about-2-h">{{negocjacje-about-2-h}}</h3>
                            <p class="jp-elem" id="negocjacje-about-2-p">{{negocjacje-about-2-p}}</p>                                                       
                        </div>
                    </div>
                </div>
                <div class="row items">
                    <div class="col-lg-9 col-lg-offset-3 col-md-9 col-md-offset-3 col-sm-12 col-xs-24 item">
                        <div class="col-md-5 col-sm-6 col-xs-24"><img src="img/sprawdzanie_dokumentacji.png" alt=""/></div>
                        <div class="col-md-19 col-sm-18 col-xs-24 jp-edit">
                            <h3 class="jp-elem" id="negocjacje-about-3-h">{{negocjacje-about-3-h}}</h3>
                            <p class="jp-elem" id="negocjacje-about-3-p">{{negocjacje-about-3-p}}</p>                           
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-24 item">
                        <div class="col-md-5 col-sm-6 col-xs-24"><img src="img/podwojnyzysk.png" alt=""/></div>
                        <div class="col-md-19 col-sm-18 col-xs-24 jp-edit">
                            <h3 class="jp-elem" id="negocjacje-about-4-h">{{negocjacje-about-4-h}}</h3>
                            <p class="jp-elem" id="negocjacje-about-4-p">{{negocjacje-about-4-p}}</p>                           
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="town">
            <div class="town-color">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-24">
                            <h1 class="jp-edit jp-elem" id="negocjacje-wspolpraca-h">Dlaczego warto z&nbsp;nami <strong>współpracować</strong>?</h1>
                        </div>
                    </div>
                    <div class="row jp-edit">
                        <div class="col-md-6 col-sm-12 col-xs-24 item">
                            <img src="img/miasto_chinski.png" alt=""/>
                            <p class="jp-elem" id="negocjacje-wspolpraca-1-p">{{negocjacje-wspolpraca-1-p}}</p>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-24 item">
                            <img src="img/miasto_24.png" alt=""/>
                            <p class="jp-elem" id="negocjacje-wspolpraca-2-p">{{negocjacje-wspolpraca-2-p}}</p>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-24 item">
                            <img src="img/miasto_pioro.png" alt=""/>
                            <p class="jp-elem" id="negocjacje-wspolpraca-3-p">{{negocjacje-wspolpraca-3-p}}</p>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-24 item">
                            <img src="img/miasto_samochod.png" alt=""/>
                            <p class="jp-elem" id="negocjacje-wspolpraca-4-p">{{negocjacje-wspolpraca-4-p}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="accordion">
            <div class="container">
                <div class="row">
                    <div class="col-xs-24">
                        <h1 class="jp-edit jp-elem" id="negocjacje-proces-h">Zobacz jak wygląda proces negocjacji</h1>
                    </div>
                </div>
                <div class="row items accordion-items">
                    <div class="col-md-12 jp-edit">
                        <div class="item">
                            <h2 class="jp-elem opened" id="negocjacje-proces-left-1-h">{{negocjacje-proces-left-1-h}}</h2>
                            <p class="accordion-child jp-elem" style="display: block;" id="negocjacje-proces-left-1-p">{{negocjacje-proces-left-1-p}}</p>       
                        </div>
                        <div class="item">
                            <h2 class="jp-elem" id="negocjacje-proces-left-2-h">{{negocjacje-proces-left-2-h}}</h2>
                            <p class="accordion-child jp-elem" id="negocjacje-proces-left-2-p">{{negocjacje-proces-left-2-p}}</p>                            
                        </div>
                        <div class="item">
                            <h2 class="jp-elem" id="negocjacje-proces-left-3-h">{{negocjacje-proces-left-3-h}}</h2>
                            <p class="accordion-child jp-elem" id="negocjacje-proces-left-3-p">{{negocjacje-proces-left-3-p}}</p> 
                        </div>
                    </div>
                    <div class="col-md-12 jp-edit">
                        <div class="item">
                            <h2 class="jp-elem opened" id="negocjacje-proces-right-1-h">{{negocjacje-proces-right-1-h}}</h2>
                            <p class="accordion-child jp-elem" style="display: block;" id="negocjacje-proces-right-1-p">{{negocjacje-proces-right-1-p}}</p>       
                        </div>
                        <div class="item">
                            <h2 class="jp-elem" id="negocjacje-proces-right-2-h">{{negocjacje-proces-right-2-h}}</h2>
                            <p class="accordion-child jp-elem" id="negocjacje-proces-right-2-p">{{negocjacje-proces-right-2-p}}</p>                            
                        </div>
                        <div class="item">
                            <h2 class="jp-elem" id="negocjacje-proces-right-3-h">{{negocjacje-proces-right-3-h}}</h2>
                            <p class="accordion-child jp-elem" id="negocjacje-proces-right-3-p">{{negocjacje-proces-right-3-p}}</p> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="whole-photo">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-24 hidden-lg subsection-photo"></div>
                    <div class="col-lg-11 col-lg-offset-12 col-md-24 subsection-text">
                        <div class="text jp-edit">
                            <h1 class="jp-elem" id="negocjacje-koszt-h">{{negocjacje-koszt-h}}</h1>
                            <p class="jp-elem" id="negocjacje-koszt-p-1">{{negocjacje-koszt-p-1}}</p>
                            <p>{{negocjacje-koszt-p-2-jp}} <strong>{{negocjacje-koszt-p-3-jp}}</strong> {{negocjacje-koszt-p-4-jp}}</p>
                            <div class="check-it jp-elem" id="negocjacje-koszt-check">{{negocjacje-koszt-check}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>        
        <section class="contact" id="contact">
            <div class="contact-fill">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 jp-edit">
                        <h1 class="jp-elem" id="index-footer-h1">{{index-footer-h1}}</h1>
                        <div class="contact-item phone">
                            <img src="img/telefon_kontakt.png" alt=""/>
                            <div>
                                <p><a href="{{index-footer-tel1-href}}" class="jp-elem" id="index-footer-tel1">{{index-footer-tel1-text}}</a></p>
                                <p><a href="{{index-footer-tel1-href}}" class="jp-elem" id="index-footer-tel2">{{index-footer-tel2-text}}</a></p>                                
                            </div>
                        </div>
                        <div class="contact-item mail">
                            <img src="img/kopertka_mail_kontakt.png" alt=""/>
                            <div>
                                <p><a href="{{index-footer-email-href}}" class="jp-elem" id="index-footer-email">{{index-footer-email-text}}</a></p>                                                             
                            </div>
                        </div>
                        <div class="contact-item address">
                            <img src="img/lokalizacja_kontakt.png" alt=""/>
                            <div>
                                <p class="jp-elem" id="index-footer-addr1">{{index-footer-addr1}}</p>                                                             
                                <p class="jp-elem" id="index-footer-addr2">{{index-footer-addr2}}</p>                                                             
                            </div>
                        </div>
                    </div>
                    <form class="col-sm-12 col-lg-8 col-lg-offset-4 hidden-xs contact-form" id="contact-form" role="form" action="" method="post">
                        <input type="hidden" name="form" value="kontakt"/>
                        <h1>Napisz do nas</h1>
                        <p><input type="text" name="nazwisko" placeholder="Imię i nazwisko"/></p>
                        <p><input type="text" name="telefon" placeholder="Numer telefonu"/></p>
                        <p><input type="text" name="email" placeholder="E-mail"/></p>
                        <p><input type="submit" value="wyślij"/></p>
                        <!--input name="warunki" class="first" type="checkbox" value="tak"/> Akceptuję warunki
                        <input name="zgoda" type="checkbox" value="tak"/> Wyrażam zgodę-->
                        <input name="regulamin" type="checkbox" value="tak"/> Zapoznałem się z <a href="attch/regulamin-strony.pdf">regulaminem</a> / akceptuję <a href="attch/regulamin-strony.pdf">regulamin</a>
                    </form>
                </div>
            </div>         
        </div>
        </section>
        <div class="map" id="map-canvas"></div>        

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul>
                            <li><a href="#">2015 Negocjatornia</a></li>
                            <li><a href="attch/polityka-prywatnosci.pdf" target="_blank">Polityka prywatności</a></li>
                            <li><a href="attch/regulamin-strony.pdf" target="_blank">Regulamin</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-12">
                        <ul class="pull-right">
                            <li>wykonanie:</li>
                            <li><a href="http://www.960pikseli.pl">www.960pikseli.pl</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </footer>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDkR1lw4SMGMFquqdryC6eBE-88HpNvcM"></script>        
        <script src="js/jquery.min.js"></script>        
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/velocity.min.js"></script>
        <script src="js/carousel.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/messages_pl.min.js"></script> 
        <script src="js/eventSpy.js"></script>
        <script src="js/player.js"></script>
<?php jp_edit(); ?>
    </body>
</html>