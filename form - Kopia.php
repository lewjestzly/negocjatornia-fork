﻿<html>
<head>
        <title>Negocjatornia</title>
        <link rel="shortcut icon" type="image/png" href="img/Favicon_16x16.png">                
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/form.css">
</head>
<body>
<div class="container">
				<div class="row header">
					<div class="col-xs-24 col-md-12 vcenter">
	                  <h1>Wypełnij formularz - uzyskasz informacje:</h1>
	                  <p>Na temat swojej aktualnej systuacji finansowej. Dokonamy pełnej analizy zdolności kredytowej, a w razie braku zaoferujemy rozwiązania by ją naprawić.</p>
					</div><!--
					--><div class="col-xs-24 col-md-12 numer vcenter">
						<small>+48</small> 790 790 190
					</div>
				</div>
                <div class="row">
                <form role="form" id="credit_form" action="" method="post">
                <input type="hidden" name="form" value="kredyt">
                    <div id="form_inputs" class="clearfix">
                        <div class="col-xs-24 col-md-12">
                      <div class="form-group clearfix">
                        <label for="input_imie" class="col-md-24 control-label">Imię</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_imie" name="form_imie">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_nazwisko" class="col-md-24 control-label">Nazwisko</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_nazwisko" name="form_nazwisko">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_email" class="col-md-24 control-label">E-mail</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_email" name="form_email">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_telefon" class="col-md-24 control-label">Nr. Telefonu</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_telefon" name="form_telefon">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_cel" class="col-md-24 control-label">Cel kredytu</label>
                        <div class="col-md-24">
                            <select class="form-control formext" id="input_cel" name="form_cel">
                                <option value="mieszkanie w MDM" class="formext_mdm">mieszkanie w MDM</option>
                                <option value="dom w MDM" class="formext_mdm">dom w MDM</option>
                                <option value="mieszkanie">mieszkanie</option>
                                <option value="budowa domu">budowa domu</option>
                                <option value="budowa domu + zakup działki">budowa domu + zakup działki</option>
                                <option value="zakup działki">zakup działki</option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group clearfix formext_add" id="formext_mdm">
                        <label for="input_miejscowosc" class="col-md-24 control-label">Miejscowość</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_miejscowosc" name="form_miejscowosc">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_zabezpiecznie" class="col-md-24 control-label">Wartość zabezpieczenia</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_zabezpiecznie" name="form_zabezpiecznie">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_wykonczenie" class="col-md-24 control-label">Wykończenie</label>
                        <div class="col-md-24">
                            <select class="form-control formext" id="input_wykonczenie" name="form_wykonczenie">
                                <option value="Tak" class="formext_wyk">Tak</option>
                                <option value="Nie" selected>Nie</option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group clearfix formext_add" id="formext_wyk">
                        <label for="input_wykonczenie_kwota" class="col-md-24 control-label">Kwota</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_wykonczenie_kwota" name="form_wykonczenie_kwota">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_kwota_kredytu" class="col-md-24 control-label">Kwota kredytu</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_kwota_kredytu" name="form_kwota_kredytu">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_wklad_wlasny" class="col-md-24 control-label">Posiadany wkład własny</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_wklad_wlasny" name="form_wklad_wlasny">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_metraz" class="col-md-24 control-label">Metraż</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_metraz" name="form_metraz">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label class="col-md-24 control-label">Co wchodzi w skład ceny nieruchomości</label>
                        <div class="col-md-24">
                            <label for="input_chbx_sklad_1">Mieszkanie/ dom <input id="input_chbx_sklad_1" type="checkbox" class="form-control" name="form_sklad[]" value="Mieszkanie/ dom"></label>
                            <label for="input_chbx_sklad_2"><input id="input_chbx_sklad_2" type="checkbox" class="form-control" name="form_sklad[]" value="segment"> segment</label>
                            <label for="input_chbx_sklad_3"><input id="input_chbx_sklad_3" type="checkbox" class="form-control" name="form_sklad[]" value="garaż"> garaż</label>
                            <label for="input_chbx_sklad_4"><input id="input_chbx_sklad_4" type="checkbox" class="form-control" name="form_sklad[]" value="komórka lokatorska"> komórka lokatorska</label>
                            <label for="input_chbx_sklad_5"><input id="input_chbx_sklad_5" type="checkbox" class="form-control" name="form_sklad[]" value="miejsce postojowe">miejsce postojowe</label>
                            <label for="input_sklad"><input type="checkbox" class="form-control formext formext_sklin" id="input_sklad" name="form_sklad[]" value="inne">inne</label>
                            <div class="formext_add" id="formext_sklin">
                              <input type="text" class="form-control" name="form_sklad_inne" placeholder="inne">
                            </div>
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_rynek" class="col-md-24 control-label">Rynek</label>
                        <div class="col-md-24">
                            <select class="form-control" id="input_rynek" name="form_rynek">
                                <option value="pierwotny">pierwotny</option>
                                <option value="wtórny">wtórny</option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_data_urodzenia" class="col-md-24 control-label">Data urodzenia kredytobiorcy</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_data_urodzenia" name="form_data_urodzenia" placeholder="mm-rrrr">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_czdzialalnosc" class="col-md-24 control-label">Czy jest prowadzona działalność gospodarcza</label>
                        <div class="col-md-24">
                            <select class="form-control formext" id="input_czdzialalnosc" name="form_czdzialalnosc">
                              <option value="Tak" class="formext_dzgosp">Tak</option>
                              <option value="Nie" selected>Nie</option>
                          </select>
                        </div>
                      </div>
                    <div class="formext_add" id="formext_dzgosp">

                      <div class="form-group clearfix">
                        <label for="input_dzialalnosc" class="col-md-24 control-label">Działalność</label>
                        <div class="col-md-24">
                            <select class="form-control formext" id="input_dzialalnosc" name="form_dzialalnosc">
                                <option value="zasady ogólne" class="formext_zas">zasady ogólne</option>
                                <option value="ryczałt" class="formext_rycz">ryczałt 3%, ryczałt 5,5%, ryczałt 8,5%, ryczałt 17% i ryczałt 20%</option>
                                <option value="pełna księgowość" class="formext_zas">pełna księgowość</option>
                                <option value="rolnik" class="formext_zas">rolnik</option>
                            </select>
                        </div>
                      </div>

                      <div class="formext_add" id="formext_rycz">

                      <div class="form-group clearfix">
                        <label for="input_rycz_2015_mc" class="col-md-24 control-label">Przychód za rok 2015 od stycznia do</label>
                        <div class="col-md-24">
                            <select class="form-control formext" id="input_rycz_2015_mc" name="form_rycz_2015_mc">
                                <option value="styczeń">styczeń</option>
                                <option value="luty">luty</option>
                                <option value="marzec">marzec</option>
                                <option value="kwiecień">kwiecień</option>
                                <option value="maj">maj</option>
                                <option value="czerwiec">czerwiec</option>
                                <option value="lipiec">lipiec</option>
                                <option value="sierpień">sierpień</option>
                                <option value="wrzesień">wrzesień</option>
                                <option value="październik">październik</option>
                                <option value="listopad">listopad</option>
                                <option value="grudzień">grudzień</option>
                            </select>
                            <input type="text" class="form-control" id="input_rycz_2015" name="form_rycz_2015">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_rycz_2014" class="col-md-24 control-label">Przychód za rok 2014</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_rycz_2014" name="form_rycz_2014">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_rycz_2013" class="col-md-24 control-label">Przychód za rok 2013</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_rycz_2013" name="form_rycz_2013">
                        </div>
                      </div>

                      </div>

                      <div class="formext_add" id="formext_zas">

                      <div class="form-group clearfix">
                        <label for="input_zas_2015_mc" class="col-md-24 control-label">Przychód z działalności za rok 2015 od stycznia do</label>
                        <div class="col-md-24">
                            <select class="form-control" id="input_zas_2015_mc" name="form_zas_2015_mc">
                                <option value="styczeń">styczeń</option>
                                <option value="luty">luty</option>
                                <option value="marzec">marzec</option>
                                <option value="kwiecień">kwiecień</option>
                                <option value="maj">maj</option>
                                <option value="czerwiec">czerwiec</option>
                                <option value="lipiec">lipiec</option>
                                <option value="sierpień">sierpień</option>
                                <option value="wrzesień">wrzesień</option>
                                <option value="październik">październik</option>
                                <option value="listopad">listopad</option>
                                <option value="grudzień">grudzień</option>
                            </select>
                            <input type="text" class="form-control" id="input_zas_2015" name="form_zas_2015">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_zas_2015_mc_koszty" class="col-md-24 control-label">Koszty z działalności za rok 2015 od stycznia do</label>
                        <div class="col-md-24">
                            <select class="form-control" id="input_zas_2015_mc_koszty" name="form_zas_2015_mc_koszty">
                                <option value="styczeń">styczeń</option>
                                <option value="luty">luty</option>
                                <option value="marzec">marzec</option>
                                <option value="kwiecień">kwiecień</option>
                                <option value="maj">maj</option>
                                <option value="czerwiec">czerwiec</option>
                                <option value="lipiec">lipiec</option>
                                <option value="sierpień">sierpień</option>
                                <option value="wrzesień">wrzesień</option>
                                <option value="październik">październik</option>
                                <option value="listopad">listopad</option>
                                <option value="grudzień">grudzień</option>
                            </select>
                            <input type="text" class="form-control" id="input_zas_2015_koszty" name="form_zas_2015">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_zas_2014" class="col-md-24 control-label">Przychód  z działalności za rok 2014</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_zas_2014" name="form_zas_2014">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_zas_2014_koszty" class="col-md-24 control-label">Koszty  z działalności za rok 2014</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_zas_2014_koszty" name="form_zas_2014_koszty">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_zas_2013" class="col-md-24 control-label">Przychód  z działalności za rok 2013</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_zas_2013" name="form_zas_2013">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_zas_2013_koszty" class="col-md-24 control-label">Koszty  z działalności za rok 2013</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_zas_2013_koszty" name="form_zas_2013_koszty">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_zas_podatek" class="col-md-24 control-label">Podatek</label>
                        <div class="col-md-24">
                            <select class="form-control" id="input_zas_podatek" name="form_zas_podatek">
                                <option value="liniowy">liniowy</option>
                                <option value="progowy">progowy</option>
                            </select>
                        </div>
                      </div>

                      </div>
                    </div>
                     </div>
                    <div class="col-xs-24 col-md-12">
                      <div class="form-group clearfix">
                        <label for="input_dochody" class="col-md-24 control-label">Z jakiego tytułu są dochody</label>
                        <div class="col-md-24">
                            <select class="form-control formext" id="input_wykonczenie" name="form_dochody">
                                <option value="umowa o pracę czas określony" class="formext_doch">umowa o pracę czas określony</option>
                                <option value="umowa o pracę czas nieokreślony">umowa o pracę czas nieokreślony</option>
                                <option value="umowa o dzieło" class="formext_doch">umowa o dzieło</option>
                                <option value="umowa zlecenie" class="formext_doch">umowa zlecenie</option>
                                <option value="kontrakt menadżerski/marynarski" class="formext_doch">kontrakt menadżerski/marynarski</option>
                                <option value="emerytura" class="formext_doch">emerytura</option>
                                <option value="renta" class="formext_doch">renta</option>
                                <option value="urlop macierzyński" class="formext_doch">urlop macierzyński</option>
                                <option value="inne" class="formext_doch formext_doch_inne">inne</option>
                            </select>
                            <input type="text" class="form-control"  class="formext_add" id="formext_doch_inne" name="form_dochody_inne" placeholder="inne">
                        </div>
                      </div>
                    <div class="formext_add">
                      <div class="form-group clearfix">
                        <label for="input_dochody_od" class="col-md-24 control-label">od kiedy</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_dochody_od" name="form_dochody_od" placeholder="mm-rrrr">
                        </div>
                      </div>


                      <div class="form-group clearfix" class="formext_add" id="formext_doch">
                        <label for="input_dochody_do" class="col-md-24 control-label">do kiedy</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_dochody_do" name="form_dochody_do" placeholder="mm-rrrr">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_dochody_ktora" class="col-md-24 control-label">która to umowa z kolei</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_dochody_ktora" name="form_dochody_ktora">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_dochody_3m" class="col-md-24 control-label">dochód netto z 3 ostatnich miesięcy</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_dochody_3m" name="form_dochody_3m">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_dochody_6m" class="col-md-24 control-label">dochód netto z 6 ostatnich miesięcy</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_dochody_6m" name="form_dochody_6m">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_dochody_12m" class="col-md-24 control-label">dochód netto z 12 ostatnich miesięcy</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_dochody_12m" name="form_dochody_12m">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_waluta" class="col-md-24 control-label">W jakiej walucie dochód</label>
                        <div class="col-md-24">
                            <select class="form-control" id="input_waluta" name="form_waluta">
                                <option value="PLN">PLN</option>
                                <option value="EUR">EUR</option>
                                <option value="GBP">GBP</option>
                                <option value="CHF">CHF</option>
                                <option value="NOK">NOK</option>
                                <option value="SEK">SEK</option>
                                <option value="USD">USD</option>
                            </select>
                        </div>
                      </div>
                    </div>
                      <div class="form-group clearfix">
                        <label for="input_2_zrodlo" class="col-md-24 control-label">Wynagrodzenie z drugiego źródła dochodu</label>
                        <div class="col-md-24">
                            <select class="form-control formext" id="input_2_zrodlo" name="form_2_zrodlo">
                              <option value="Tak" class="formext_drzr">Tak</option>
                              <option value="Nie" selected>Nie</option>
                            </select>
                        </div>
                      </div>
                      
                      <div class="formext_add" id="formext_drzr" class="formext_add">
                      
                      <div class="form-group clearfix">
                        <label for="input_2dochody" class="col-md-24 control-label">Z jakiego tytułu są dochody</label>
                        <div class="col-md-24">
                            <select class="form-control formext" id="input_2wykonczenie" name="form_2dochody">
                                <option value="umowa o pracę czas określony" class="formext_2doch">umowa o pracę czas określony</option>
                                <option value="umowa o pracę czas nieokreślony">umowa o pracę czas nieokreślony</option>
                                <option value="umowa o dzieło" class="formext_2doch">umowa o dzieło</option>
                                <option value="umowa zlecenie" class="formext_2doch">umowa zlecenie</option>
                                <option value="kontrakt menadżerski/marynarski" class="formext_2doch">kontrakt menadżerski/marynarski</option>
                                <option value="emerytura" class="formext_2doch">emerytura</option>
                                <option value="renta" class="formext_2doch">renta</option>
                                <option value="urlop macierzyński" class="formext_2doch">urlop macierzyński</option>
                                <option value="inne" class="formext_2doch formext_2doch_inne">inne</option>
                            </select>
                            <input type="text" class="form-control"  class="formext_add" id="formext_2doch_inne" name="form_2dochody_inne" placeholder="inne">
                        </div>
                      </div>
                    
                      <div class="form-group clearfix">
                        <label for="input_2dochody_od" class="col-md-24 control-label">od kiedy</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_2dochody_od" name="form_2dochody_od" placeholder="mm-rrrr">
                        </div>
                      </div>


                      <div class="form-group clearfix" class="formext_add" id="formext_2doch">
                        <label for="input_2dochody_do" class="col-md-24 control-label">do kiedy</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_2dochody_do" name="form_2dochody_do" placeholder="mm-rrrr">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_2dochody_ktora" class="col-md-24 control-label">która to umowa z kolei</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_2dochody_ktora" name="form_2dochody_ktora">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_2dochody_3m" class="col-md-24 control-label">dochód netto z 3 ostatnich miesięcy</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_2dochody_3m" name="form_2dochody_3m">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_2dochody_6m" class="col-md-24 control-label">dochód netto z 6 ostatnich miesięcy</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_2dochody_6m" name="form_2dochody_6m">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_2dochody_12m" class="col-md-24 control-label">dochód netto z 12 ostatnich miesięcy</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_2dochody_12m" name="form_2dochody_12m">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_2dochody_waluta" class="col-md-24 control-label">W jakiej walucie dochód</label>
                        <div class="col-md-24">
                            <select class="form-control" id="input_2dochody_waluta" name="form_2dochody_waluta">
                                <option value="PLN">PLN</option>
                                <option value="EUR">EUR</option>
                                <option value="GBP">GBP</option>
                                <option value="CHF">CHF</option>
                                <option value="NOK">NOK</option>
                                <option value="SEK">SEK</option>
                                <option value="USD">USD</option>
                            </select>
                        </div>
                      </div>

                      </div>

                      <div class="form-group clearfix">
                        <label for="input_utrzymanie_dorosle" class="col-md-24 control-label">Ilość osób na utrzymaniu</label>
                        <div class="col-md-24 formext_add">
                            osoby dorosłe <input type="text" class="form-control" id="input_utrzymanie_dorosle" name="form_utrzymanie_dorosle">
                            dzieci
                            <input type="text" class="form-control" id="input_utrzymanie_dzieci" name="form_utrzymanie_dzieci">
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_kredyty" class="col-md-24 control-label">Posiadane kredyty?</label>
                        <div class="col-md-24">
                            <select class="form-control formext"id="input_kredyty" name="form_kredyty">
                                <option value="" class="formext_krd" selected>Wybierz opcje</option>
                                <option value="Tak">Tak</option>
                                <option value="Nie">Nie</option>
                            </select>
                        </div>
                      </div>
                        <div class="formext_add" id="formext_krd">
                            
                      <div class="form-group clearfix">
                        <label for="input_krd_rata" class="col-md-24 control-label">Miesięczna rata (suma rat)</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_krd_rata" name="form_krd_rata">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_krd_pozostalo" class="col-md-24 control-label">Pozostała kwota do spłaty</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_krd_pozostalo" name="form_krd_pozostalo">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_krd_zaleglosci" class="col-md-24 control-label">Czy były zaległości w spłatach?</label>
                        <div class="col-md-24">
                            <select class="form-control" id="input_krd_zaleglosci" name="form_krd_zaleglosci">
                                <option value="tak">tak</option>
                                <option value="nie">nie</option>
                            </select>
                        </div>
                      </div>

                        </div>


                      <div class="form-group clearfix">
                        <label for="input_posiadanie_ror" class="col-md-24 control-label">Posiadanie Rachunku Oszczędnościowo-Rozliczeniowego z przyznanym limitem kredytowym</label>
                        <div class="col-md-24">
                            <select class="form-control formext" id="input_posiadanie_ror" name="form_posiadanie_ror">
                                <option value="Tak" class="formext_ror">Tak</option>
                                <option value="Nie" selected>Nie</option>
                            </select>
                        </div>
                      </div>

                      <div class="form-group clearfix formext_add" id="formext_ror">
                        <label for="input_posiadanie_ror_limit" class="col-md-24 control-label">Przyznany limit</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_posiadanie_ror_limit" name="form_posiadanie_ror_limit">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_posiadanie_karty" class="col-md-24 control-label">Posiadanie Karty Kredytowej</label>
                        <div class="col-md-24">
                            <select class="form-control formext" id="input_posiadanie_karty" name="form_posiadanie_karty">
                                <option value="Tak" class="formext_krtkrd">Tak</option>
                                <option value="Nie" selected>Nie</option>
                            </select>
                        </div>
                      </div>

                      <div class="form-group clearfix formext_add" id="formext_krtkrd">
                        <label for="input_posiadanie_karty_limit" class="col-md-24 control-label">Przyznany limit</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_posiadanie_karty_limit" name="form_posiadanie_karty_limit">
                        </div>
                      </div>

                      <div class="form-group clearfix">
                        <label for="input_posiadanie_samochodu" class="col-md-24 control-label">Posiadane samochody</label>
                        <div class="col-md-24">
                            <select class="form-control" id="input_posiadanie_samochodu" name="form_posiadanie_samochodu">
                                <option value="Tak">Tak</option>
                                <option value="Nie">Nie</option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group clearfix">
                        <label for="input_posiadane_rach_bankowe" class="col-md-24 control-label">Posiadane rachunki bankowe (w jakich bankach)</label>
                        <div class="col-md-24">
                            <input type="text" class="form-control" id="input_posiadane_rach_bankowe" name="form_posiadane_rach_bankowe">
                        </div>
                      </div>
                      <div class="form-group clearfix form_nocopy">
                        <label for="input_drugi_kredytobiorca" class="col-md-24 control-label">Drugi kredytobiorca</label>
                        <div class="col-md-24">
                            <select class="form-control formnew" id="input_drugi_kredytobiorca" name="form_drugi_kredytobiorca">
                                <option value="Tak" class="formext_new">Tak</option>
                                <option value="Nie" selected>Nie</option>
                            </select>
                        </div>
                      </div>
                        </div>
                    </div>

                    <div class="col-xs-24 div-submit"><button type="submit" class="form-submit">Wyślij formularz, a dowiesz się jaki kredyt możesz uzyskać</button></div>
                    <div class="col-xs-24 checkboxes">
                     <label class="form-control chx" for="input_akceptuje"> <span>Akceptuję <a href="attch/regulamin-strony.pdf">regulamin strony</a></span>  <input type="checkbox" id="input_akceptuje" name="form_akceptuje" value="Tak"></label> 
                    <label class="form-control chx" for="input_zgoda"><span>Zgoda&nbsp;na&nbsp;przetwarzanie danych osobowych</span> <input type="checkbox" id="input_zgoda" name="form_zgoda" value="Tak"></label>
                    </div>
                    <input type="hidden" name="time" value="<?=time()?>">
                </form>
                </div>
         
         </div>
        <script src="js/jquery.min.js"></script> 
        <script src="js/bootstrap.min.js"></script>      
        <script src="js/form.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/messages_pl.min.js"></script> 
        <script src="js/form_valid.js"></script>
</body>
</html>