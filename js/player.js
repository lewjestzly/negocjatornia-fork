function loadVideo(){    
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/player_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);    
    loaded = true;
}

var loaded = false;
var played = false;
var player;
var apiReady = false;
var link = 'https://www.youtube.com/watch?v=dttqTkmD9-I';

function onYouTubeIframeAPIReady() {
    apiReady = true;     
}

function startVideoPlayer(someId, someUrl) {
    if (apiReady) {
        played = true;
        player = new YT.Player(someId, {
            videoId: returnVideoID(someUrl),
            events: {
                'onReady': onPlayerReady,
                'onStateChange': function (event) {
                    if (event.data == "0") {                            
                        $('.screen-container').fadeOut();
                    }
                }
            }
        });
    }
    else{
        $('.screen-container').fadeOut();
    }
}

function onPlayerReady() {        
    player.playVideo();
}
    
$(document).ready(function(){    
    $('.tv img').click(function(){
        if($(window).width()>=1024){
            if(!player){                
                $('.screen-container').fadeIn();    
                startVideoPlayer('screen', link);                   
            }
            else{
                $('.screen-container').fadeIn();    
                player.playVideo();
            }

        }
        else{
            window.open(link);
        }                
    });
    
    if($(window).width()>=1024){        
        loadVideo();
    }
});

function returnVideoID(url) {
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match && match[7].length == 11) {
        return match[7];
    } else {
        console.log("nieprawidłowy url");
        $('.screen-container').fadeOut();
    }
}

window.addEventListener("resize", function () {
    if(!loaded){
        if($(window).width()>=1024){        
            loadVideo();
        }        
    }
}, false);
