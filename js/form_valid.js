$(document).ready(function(){

	$.validator.addMethod("regx", function(value, element, regexpr) {
    		return regexpr.test(value);
		}, "Proszę podać poprawne dane.");

			$("#credit_form").validate({

			  submitHandler: function(form) {
 					$.post('php/form_submit.php',$(form).serialize(),function(data){
                        $('.form-submit').text(data).attr('disabled','true');
                        $.post('php/pdf_generator.php',$(form).serialize(),function(data){
                        	
                        var time = $('input[name=time]').val();
                        $('.form-submit').after('<br><a href="php/negocjatornia_'+time+'.pdf" target="_blank">Pobierz formularz</a>');
                        });
                    });
			  },
			rules: {
				form_imie: "required",
				form_nazwisko: "required",
				form_email: {
					required: true,
					email: true
				},
				form_telefon: {
					required: true,
					digits: true
				},
				form_cel: {required:true},
				form_miejscowosc: {
					required: function() {
						return ( $('[name="form_cel"]').val()=="mieszkanie w MDM" || $('[name="form_cel"]').val()=="dom w MDM" );
					}
				},
				form_zabezpiecznie: {
                                    required: true
                                },
				form_wykonczenie: {
                                    required:true
                                },
				form_wykonczenie_kwota: {
					required: function() {
						return ( $('[name="form_wykonczenie"]').val()=="Tak" );
					},
                                        
					number: true
				},
				form_kwota_kredytu: {required: true,
					number: true
				},
                                form_okres_kredytowania: {
					required: true,
				},
				form_raty:{
                                        required: true,
                                },
				form_wklad_wlasny: {required: true,
					number: true
				},
				form_metraz: {
					required: true,
					number: true
				},
				'form_sklad[]': {
                                        required: true,
                                         minlength: 1
				},
				form_sklad_inne: {
					required: "#input_sklad:checked"
				},
				form_rynek: "required",
				form_data_urodzenia: {
					required: true,
					regx: /^(1[0-2]|0[1-9]|\d)\-(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)$/
				},
				form_czdzialalnosc: "required",
				form_dzialalnosc: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" );
					}
				},
				form_rycz_2015_mc: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" && $('[name="form_dzialalnosc"]').val()=="ryczałt" );
					}
				},
				form_rycz_2015: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" && $('[name="form_dzialalnosc"]').val()=="ryczałt" );
					},
					number: true
				},
				form_rycz_2014: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" && $('[name="form_dzialalnosc"]').val()=="ryczałt" );
					},
					number: true
				},
				form_rycz_2013: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" && $('[name="form_dzialalnosc"]').val()=="ryczałt" );
					},
					number: true
				},


				form_zas_2015_mc: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" && $('[name="form_dzialalnosc"]').val()=="zasady ogólne" );
					}
				},
				form_zas_2015: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" && $('[name="form_dzialalnosc"]').val()=="zasady ogólne" );
					},
					number: true
				},
				form_zas_2015_mc_koszty: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" && $('[name="form_dzialalnosc"]').val()=="zasady ogólne" );
					},
				},
				form_zas_2015_koszty: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" && $('[name="form_dzialalnosc"]').val()=="zasady ogólne" );
					},
					number: true
				},
				form_zas_2014: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" && $('[name="form_dzialalnosc"]').val()=="zasady ogólne" );
					},
					number: true
				},
				form_zas_2014_koszty: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" && $('[name="form_dzialalnosc"]').val()=="zasady ogólne" );
					},
					number: true
				},
				form_zas_2013: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" && $('[name="form_dzialalnosc"]').val()=="zasady ogólne" );
					},
					number: true
				},
				form_zas_2013_koszty: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" && $('[name="form_dzialalnosc"]').val()=="zasady ogólne" );
					},
					number: true
				},
				form_zas_podatek: {
					required: function() {
						return ( $('[name="form_czdzialalnosc"]').val()=="Tak" && $('[name="form_dzialalnosc"]').val()=="zasady ogólne" );
					},
					number: true
				},

				form_dochody: {
					required: true
				},
				form_dochody_inne: {
					required: function() {
						return ( $('[name="form_dochody"]').val()=="inne" );
					}
				},
				form_dochody_od: {
					required: true,
					regx: /^(1[0-2]|0[1-9]|\d)\-(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)$/
				},
				form_dochody_do: {
					required: function() {
						return !( $('[name="form_dochody"]').val()=="umowa o pracę czas nieokreślony" );
					},
					regx: /^(1[0-2]|0[1-9]|\d)\-(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)$/
				},
				form_dochody_ktora:{
                                        
					required: true,
					digits: true
				},
				form_dochody_3m: {
					required: true,
					number: true
				},
				form_dochody_6m: {
					required: true,
					number: true
				},
				form_dochody_12m: {
					required: true,
					number: true
				},
				form_dochody_waluta: "required",

				form_2_zrodlo: {required:true },

				form_2dochody: {
					required: function() {
						return ( $('[name="form_2_zrodlo"]').val()=="Tak" );
					}
				},
				form_2dochody_inne: {
					required: function() {
						return ( $('[name="form_2dochody"]').val()=="inne" && $('[name="form_2_zrodlo"]').val()=="Tak" );
					}
				},
				form_2dochody_od: {
					required: function() {
						return ( $('[name="form_2_zrodlo"]').val()=="Tak" );
					},
					regx: /^(1[0-2]|0[1-9]|\d)\-(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)$/
				},
				form_2dochody_do: {
					required: function() {
						return !( $('[name="form_2dochody"]').val()=="umowa o pracę czas nieokreślony" && $('[name="form_2_zrodlo"]').val()=="Tak" );
					},
					regx: /^(1[0-2]|0[1-9]|\d)\-(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)$/
				},
				form_2dochody_ktora: {
					required: function() {
						return ( $('[name="form_2_zrodlo"]').val()=="Tak" );
					},
					digits:true

				},
				form_2dochody_3m: {
					required: function() {
						return ( $('[name="form_2_zrodlo"]').val()=="Tak" );
					},
					number: true
				},
				form_2dochody_6m: {
					required: function() {
						return ( $('[name="form_2_zrodlo"]').val()=="Tak" );
					},
					number: true
				},
				form_2dochody_12m: {
					required: function() {
						return ( $('[name="form_2_zrodlo"]').val()=="Tak" );
					},
					number: true
				},
				form_2dochody_waluta: {
					required: function() {
						return ( $('[name="form_2_zrodlo"]').val()=="Tak" );
					}
				},

				form_utrzymanie_dorosle: {
					required: true,
					minlength: 1,
					digits: true
				},
				form_utrzymanie_dzieci: {
					required: true,
					minlength: 1,
					digits: true
				},

				form_kredyty: {
					required: true,
				},
				form_krd_rata: {
					required: function() {
						return ( $('[name="form_kredyty"]').val()=="Tak" );
					},
					number: true
				},
				form_krd_pozostalo: {
					required: function() {
						return ( $('[name="form_kredyty"]').val()=="Tak" );
					},
					number: true
				},
                                form_posiadanie_karty: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
                                        required: true
				},
                                form_posiadanie_samochodu: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
                                        
                                        required: true
				},
				form_krd_zaleglosci: {
					required: function() {
						return ( $('[name="form_kredyty"]').val()=="Tak" );
					}
				},

				form_posiadanie_ror: "required",
				form_posiadanie_ror_limit: {
					required: function() {
						return ( $('[name="form_posiadanie_ror"]').val()=="Tak" );
					},
                                       
					number: true
				},
                                form_posiadane_rach_bankowe:{
                                    required:true,
                                    
                                },
                                form_drugi_kredytobiorca:{
                                    required:true,
                                },
				
				form_imie_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},
				form_nazwisko_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},
				form_email_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
					email: true
				},
				form_telefon_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
					digits: true
				},
				form_cel_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},
				form_miejscowosc_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_cel_2"]').val()=="mieszkanie w MDM" || $('[name="form_cel_2"]').val()=="dom w MDM" );
					}
				},
				form_zabezpiecznie_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},
				form_wykonczenie_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},
				form_wykonczenie_kwota_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_wykonczenie_2"]').val()=="Tak" );
					},
					number: true
				},
				form_kwota_kredytu_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
					number: true
				},
                                form_okres_kredytowania_2:{
                                    required:true
                                },
                                form_raty_2:{
                                    required:true
                                },
                                form_wklad_wlasny_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
					number: true
				},
				form_metraz_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
					number: true
				},
				'form_sklad_2[]': {
	                required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
	                minlength: 1
				},
				form_sklad_inne_2: {
					required: "#input_sklad_2:checked"
				},
				form_rynek_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},
				form_data_urodzenia_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
					regx: /^(1[0-2]|0[1-9]|\d)\-(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)$/
				},
				form_czdzialalnosc_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},
				form_dzialalnosc_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" );
					}
				},
				form_rycz_2015_mc_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" && $('[name="form_dzialalnosc_2"]').val()=="ryczałt" );
					}
				},
				form_rycz_2015_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" && $('[name="form_dzialalnosc_2"]').val()=="ryczałt" );
					},
					number: true
				},
				form_rycz_2014_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" && $('[name="form_dzialalnosc_2"]').val()=="ryczałt" );
					},
					number: true
				},
				form_rycz_2013_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" && $('[name="form_dzialalnosc_2"]').val()=="ryczałt" );
					},
					number: true
				},


				form_zas_2015_mc_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" && $('[name="form_dzialalnosc_2"]').val()=="zasady ogólne" );
					}
				},
				form_zas_2015_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" && $('[name="form_dzialalnosc_2"]').val()=="zasady ogólne" );
					},
					number: true
				},
				form_zas_2015_mc_koszty_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" && $('[name="form_dzialalnosc_2"]').val()=="zasady ogólne" );
					},
				},
				form_zas_2015_koszty_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" && $('[name="form_dzialalnosc_2"]').val()=="zasady ogólne" );
					},
					number: true
				},
				form_zas_2014_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" && $('[name="form_dzialalnosc_2"]').val()=="zasady ogólne" );
					},
					number: true
				},
				form_zas_2014_koszty_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" && $('[name="form_dzialalnosc_2"]').val()=="zasady ogólne" );
					},
					number: true
				},
				form_zas_2013_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" && $('[name="form_dzialalnosc_2"]').val()=="zasady ogólne" );
					},
					number: true
				},
				form_zas_2013_koszty_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" && $('[name="form_dzialalnosc_2"]').val()=="zasady ogólne" );
					},
					number: true
				},
				form_zas_podatek_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_czdzialalnosc_2"]').val()=="Tak" && $('[name="form_dzialalnosc_2"]').val()=="zasady ogólne" );
					},
					number: true
				},

				form_dochody_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},
				form_dochody_inne_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_dochody_2"]').val()=="inne" );
					}
				},
				form_dochody_od_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
					regx: /^(1[0-2]|0[1-9]|\d)\-(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)$/
				},
				form_dochody_do_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_dochody_2"]').val()!=="umowa o pracę czas nieokreślony" );
					},
					regx: /^(1[0-2]|0[1-9]|\d)\-(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)$/
				},
				form_dochody_ktora_2:{
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak"  );
					},
					digits: true
				},
				form_dochody_3m_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
					number: true
				},
				form_dochody_6m_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
					number: true
				},
				form_dochody_12m_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
					number: true
				},
				form_dochody_waluta_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},

				form_2_zrodlo_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},

				form_2dochody_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_2_zrodlo_2"]').val()=="Tak" );
					}
				},
				form_2dochody_inne_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_2dochody_2"]').val()=="inne" && $('[name="form_2_zrodlo_2"]').val()=="Tak" );
					}
				},
				form_2dochody_od_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_2_zrodlo_2"]').val()=="Tak" );
					},
					regx: /^(1[0-2]|0[1-9]|\d)\-(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)$/
				},
				form_2dochody_do_2: {
					required: function() {
						return ($('[name="form_drugi_kredytobiorca"]').val()=="Tak" && !( $('[name="form_2dochody_2"]').val()=="umowa o pracę czas nieokreślony" && $('[name="form_2_zrodlo_2"]').val()=="Tak" ));
					},
					regx: /^(1[0-2]|0[1-9]|\d)\-(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)$/
				},
				form_2dochody_ktora_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_2_zrodlo_2"]').val()=="Tak" );
					},
					digits:true

				},
				form_2dochody_3m_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_2_zrodlo_2"]').val()=="Tak" );
					},
					number: true
				},
				form_2dochody_6m_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_2_zrodlo_2"]').val()=="Tak" );
					},
					number: true
				},
				form_2dochody_12m_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_2_zrodlo_2"]').val()=="Tak" );
					},
					number: true
				},
				form_2dochody_waluta_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_2_zrodlo_2"]').val()=="Tak" );
					}
				},

				form_utrzymanie_dorosle_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
					minlength: 1,
					digits: true
				},
				form_utrzymanie_dzieci_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					},
					minlength: 1,
					digits: true
				},

				form_kredyty_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},
				form_krd_rata_2: {
					required: function() {
						console.log($('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_kredyty_2"]').val()=="Tak");
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_kredyty_2"]').val()=="Tak" );
					},
					number: true
				},
				form_krd_pozostalo_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_kredyty_2"]').val()=="Tak" );
					},
					number: true
				},
				form_krd_zaleglosci_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_kredyty_2"]').val()=="Tak" );
					}
				},

				form_posiadanie_ror_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},
				form_posiadanie_ror_limit_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_posiadanie_ror_2"]').val()=="Tak" );
					},
					number: true
				},

				form_posiadanie_karty_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},
				form_posiadanie_karty_limit_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" && $('[name="form_posiadanie_karty_2"]').val()=="Tak" );
					},
					number: true
				},

				form_posiadanie_samochodu_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},
				form_posiadanie_rach_bankowe_2: {
					required: function() {
						return ( $('[name="form_drugi_kredytobiorca"]').val()=="Tak" );
					}
				},
				form_akceptuje:
				{
					required: true
				},
				form_zgoda:
				{
					required: true
				}

			},
			messages:
			{
				'form_sklad[]':
				{
					required: "Wybierz co najmniej jedno pole."
				},
				'form_sklad_2[]':
				{
					required: "Wybierz co najmniej jedno pole."
				}
			}
		});

});