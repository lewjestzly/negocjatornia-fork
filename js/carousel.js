


function getSlideId(pageNumber) {
    return '#slide-'+pageNumber;
}
var currentSlide = 0;
var swipeDuration = 700;
var swipeEasing = "easeOutCubic";
var lock = false;
var slideCount = $('.slide').length;

        
function slideLeft() {        
    if (!lock && currentSlide !== 0) {
        lock = true;
        var someSlideId = getSlideId(currentSlide-1);
        var currentSlideId = getSlideId(currentSlide);
        $(someSlideId).css({'left': '-100%', 'display': 'block'});
        $(currentSlideId).velocity({'left': '+100%'}, {duration: swipeDuration, easing: swipeEasing, complete: function () {
                $('.slide').hide();
                $(someSlideId).css({'display': 'block'});
            }});
        $(someSlideId).velocity({'left': '0'}, {duration: swipeDuration, easing: swipeEasing, complete: function () {
                lock = false;                
            }});
        currentSlide--;
    }
}

function slideRight(x) {
	if(x == undefined)
		x = 2;
    if (!lock) {
        if(currentSlide >= slideCount-1){
            lock = true;
            var someSlideId = getSlideId(0);
            var currentSlideId = getSlideId(currentSlide);

            $(someSlideId).css({'left': '100%', 'display': 'block'});
            $(currentSlideId).velocity({'left': '-100%'}, {duration: swipeDuration, easing: swipeEasing, complete: function () {
                    //$('.side').hide();
                    $(someSlideId).css({'display': 'block'});
                }});
            $(someSlideId).velocity({'left': '0'}, {duration: swipeDuration, easing: swipeEasing, complete: function () {
                    lock = false;                                

                }});
            currentSlide = 0;                    
        }
        else{
        
            lock = true;
            var someSlideId = getSlideId(currentSlide+1);
            var currentSlideId = getSlideId(currentSlide);

            $(someSlideId).css({'left': '100%', 'display': 'block'});
            $(currentSlideId).velocity({'left': '-100%'}, {duration: swipeDuration, easing: swipeEasing, complete: function () {
                    //$('.side').hide();
                    $(someSlideId).css({'display': 'block'});
                }});
            $(someSlideId).velocity({'left': '0'}, {duration: swipeDuration, easing: swipeEasing, complete: function () {
                    lock = false;                                

                }});
            currentSlide++;        
        }
    }
}

$(document).ready(function(){
    autoSlide();

});

function autoSlide(){
    setTimeout(function(){
        slideRight();
        autoSlide();
    },10000);    
}