var navDuration;

function hideNav() {
    if (!navHidden) {
        $('nav, nav img, nav ul').velocity('stop');
        $('nav').velocity({'padding-top': '15px', 'padding-bottom': '15px'});
        $('nav img').velocity({'height': '50px'});
        $('nav ul').velocity({'padding-top': '0px'});
        navHidden = true;
    }
}

function showNav() {
    if (navHidden) {
        $('nav, nav img, nav ul').velocity('stop');
        $('nav').velocity({'padding-top': '35px', 'padding-bottom': '35px'});
        $('nav img').velocity({'height': '74px'});
        $('nav ul').velocity({'padding-top': '13px'});
        navHidden = false;
    }
}

var windowWidth;
var hamburgerEaten = false;
var collapseWidth = 1051;

$(document).ready(function () {
    windowWidth = $(window).width();
    if (windowWidth <= collapseWidth) hideNav();        
    else{
        showNav();
        $('nav ul').css({'display': ''});        
    }        

    $('.about .subsection-right img').mouseenter(function(){
        $('.about .subsection-right .cloud').fadeIn();    
    });
    $('.about .subsection-right img').mouseleave(function(){
        $('.about .subsection-right .cloud').fadeOut();    
    });
    

    $('.check-offer .show-cloud').mouseenter(function(){
        $('.check-offer .cloud').fadeIn();    
    });
    $('.check-offer .show-cloud').mouseleave(function(){
        $('.check-offer .cloud').fadeOut();    
    });

    
    $('.accordion-items h2').click(function () {
        if ($(this).hasClass('opened')) {
            $(this).parent().children('.accordion-child').velocity('slideUp');
        }
        else
        {
            $(this).parent().children('.accordion-child').velocity('slideDown');
        }
        $(this).toggleClass('opened');
    });
        $('.hamburger').click(function () {
            if (hamburgerEaten)
                prepareHamburger();
            else
                eatHamburger();
        });
        
    $('a.page-scroll').click(function(event) {
        var $anchor = $(this);        
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top-90
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
        prepareHamburger();
    });
   
});



var navHidden = false;

window.onscroll = function () {
    if (windowWidth > collapseWidth) {
        if (scrollY >= 20) {
            hideNav();
        }
        else {
            showNav();
        }
    }
};

window.addEventListener("resize", function () {
    windowWidth = $(window).width();    
    if (windowWidth <= collapseWidth) hideNav();        
    else{
        showNav();
        $('nav ul').css({'display': ''});        
    }    
    setTimeout(function(){initialize();},1000);    
}, false);

/*======================
    hamburger automation
    ======================*/

function eatHamburger() {
    hamburgerEaten = true;
    $('nav ul').velocity("slideDown",{duration: 300, easing: "easeOutQuint"});

}

function prepareHamburger() {
    if (windowWidth <= collapseWidth) {
        hamburgerEaten = false;
        $('nav ul').velocity("slideUp",{duration: 300, easing: "easeOutQuint"});
    }
}

function initialize() {
    var myLatLng = new google.maps.LatLng(52.268706, 21.020935);
    if($(window).width()>=1024) var draggableMap = true;
    else var draggableMap = false;                   
    var mapOptions = {
        center: myLatLng,
        zoom: 18,
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        draggable: draggableMap,
    };
    var map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: 'img/lokalizacja_kontakt_mapa.png'
    });
    marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);


$(document).ready(function () {

            $("#contact-form").validate({  
                submitHandler: function(form) { 
                    $.post('php/form_submit.php',$(form).serialize(),function(data){
                        $(form).find('[type="submit"]').val(data).attr('disabled','true');
                    });
                },
                  rules: {
                    nazwisko: "required",
                    telefon: {required:true,
                    digits: true},
                    email: {required:true,      email: true},
                    warunki: "required",
                    zgoda: "required",
                    regulamin: "required"
                  },
                  errorPlacement: function(error, element) {
                    var el_name = $(element).attr('name');
                        if ($(element).is("[type='checkbox']")){
                            $("#regulamin-error").addClass('chbx-error');
                        }
                        else
                            error.insertAfter(element);
                    }
                });
});