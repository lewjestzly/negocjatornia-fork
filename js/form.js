$(document).ready(function(){
	var formext = [
		'mdm','wyk','rycz','zas','doch','doch_inne','krd','ror','krtkrd','sklin','dzgosp','drzr','2doch','2doch_inne',

		'mdm_2','wyk_2','rycz_2','zas_2','doch_2','doch_inne_2','krd_2','ror_2','krtkrd_2','sklin_2','dzgosp_2','drzr_2','2doch_2','2doch_inne_2',
		
                'doch_3', 'doch_3_2'
                ];

	$.each(formext,function(index,value){
		$('#formext_'+value).css('display','none');
	});
	$( "[class~='formext']" )
			.change(function () {
				$.each(formext,function(index,value)
				{
					if ($("option:selected, input:checked").hasClass("formext_"+value)) {
                                            $("#formext_"+value).slideDown('slow');
					}
					else{
                                            $("#formext_"+value).slideUp('slow');
					}
				});
			})
			.change();

	$("[class~='formnew']")
			.change(function () {
					if ($("option:selected").hasClass("formext_new")) {
						var $elements = $( "#form_inputs" )
							.children()
							.clone(true,true);
						$elements
							.find('*')
							.remove('.form_nocopy')
							.remove('label.error')
							.each(function(index,element){
									var _id = $(element).prop('id');
									var _class =$(element).prop('class');
									var _name =$(element).prop('name');
									var _for = $(element).prop('for');
									if($(element).is('input[type!=checkbox]'))
										$(element).val('');
										//alert(index + ' ' + _class.indexOf('formext'));
									if ($(element).is('[id]'))
										$(element).prop('id',_id+'_2');
                                                                        
                                                                        if ($(element).is('[class]') && $(element).hasClass('control-label') !== true && $(element).hasClass('col-md-24') !== true && $(element).hasClass('col-xs-1_2') !== true && $(element).hasClass('col-md-1_2') !== true) {
                                                                            var classList = $(element).attr('class').split(/\s+/);
                                                                            $(element).removeClass();
                                                                            $.each(classList, function(index, className) {
                                                                                $(element).addClass(className + '_2');
                                                                            });
                                                                        }

									if($(element).is("[name*='[]']"))
										$(element).prop('name',_name.replace('[]','_2[]'));
									else
										if ($(element).is('[name]'))
											$(element).prop('name',_name+'_2');

									 if ($(element).is('[for]'))
										$(element).prop('for',_for+'_2');
									
									//fix

								 	if($(element).hasClass('clearfix_2'))
								 		$(element).removeClass('clearfix_2').addClass('clearfix');
								 	if($(element).hasClass('form-control_2'))
								 		$(element).removeClass('form-control_2').addClass('form-control');
								 	if($(element).hasClass('formext_2'))
								 		$(element).removeClass('formext_2').addClass('formext');
							});
						$("#form_inputs").after('<div id="formnew"></div>');
						$("#formnew").slideUp('fast');
						$("#formnew").prepend("<h2 class=\"clearfix\">Drugi kredytobiorca</h2>");
						$elements.appendTo("#formnew");
						$("#formnew").slideDown('slow');

// 	$inputs = $('#form_inputs input, #form_inputs select, #formnew input, #formnew select');
// 	$array_name="array(";
// 	$array_text="array(";
// 		$array="array(";
// 	$.each($inputs,function(i,e){
// 		if($(e).attr('type')=='checkbox')
// 			$text = $(e).parent().parent().parent().find('label.control-label').text();
// 		else
// 			$text = $(e).parent().parent().find('label').text();
// 		$name = $(e).attr('name');
// console.log($text,$name)
// 		$array_name += "'"+$name +"',";
// 		$array_text += "'"+$text +"',";
// 		$array += "'"+$name+"'=>'"+$text+"',";
// 	});
// 	$array_name += ");";
// 	$array_text += ");";
// 	$array += ");";
	
	// $('body').prepend($array_name);
	// $('body').prepend($array_text);
	// $('body').prepend($array);
					}
					else{
						$("#formnew").slideUp("slow", function() { $(this).remove(); } );
					}
			})
			.change();

});