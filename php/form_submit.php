﻿<?php
$name_kr = array('form_imie'=>'Imię','form_nazwisko'=>'Nazwisko','form_email'=>'E-mail','form_telefon'=>'Nr. Telefonu','form_cel'=>'Cel kredytu','form_miejscowosc'=>'Miejscowość','form_zabezpiecznie'=>'Wartość zabezpieczenia','form_wykonczenie'=>'Wykończenie','form_wykonczenie_kwota'=>'Kwota','form_kwota_kredytu'=>'Kwota kredytu','form_wklad_wlasny'=>'Posiadany wkład własny','form_metraz'=>'Metraż','form_sklad[]'=>'Co wchodzi w skład ceny nieruchomości','form_sklad[]'=>'Co wchodzi w skład ceny nieruchomości','form_sklad'=>'Co wchodzi w skład ceny nieruchomości','form_sklad[]'=>'Co wchodzi w skład ceny nieruchomości','form_sklad[]'=>'Co wchodzi w skład ceny nieruchomości','form_sklad[]'=>'Co wchodzi w skład ceny nieruchomości','form_sklad_inne'=>'Mieszkanie/ dom segment garaż komórka lokatorskamiejsce postojoweinne','form_rynek'=>'Rynek','form_data_urodzenia'=>'Data urodzenia kredytobiorcy','form_czdzialalnosc'=>'Czy jest prowadzona działalność gospodarcza','form_dzialalnosc'=>'Działalność','form_rycz_2015_mc'=>'Przychód za rok 2015 od stycznia do','form_rycz_2015'=>'Przychód za rok 2015 od stycznia do','form_rycz_2014'=>'Przychód za rok 2014','form_rycz_2013'=>'Przychód za rok 2013','form_zas_2015_mc'=>'Przychód z działalności za rok 2015 od stycznia do','form_zas_2015'=>'Przychód z działalności za rok 2015 od stycznia do','form_zas_2015_mc_koszty'=>'Koszty z działalności za rok 2015 od stycznia do','form_zas_2015'=>'Koszty z działalności za rok 2015 od stycznia do','form_zas_2014'=>'Przychód z działalności za rok 2014','form_zas_2014_koszty'=>'Koszty z działalności za rok 2014','form_zas_2013'=>'Przychód z działalności za rok 2013','form_zas_2013_koszty'=>'Koszty z działalności za rok 2013','form_zas_podatek'=>'Podatek','form_dochody'=>'Z jakiego tytułu są dochody','form_dochody_inne'=>'Z jakiego tytułu są dochody','form_dochody_od'=>'od kiedy','form_dochody_do'=>'do kiedy','form_dochody_ktora'=>'która to umowa z kolei','form_dochody_3m'=>'dochód netto z 3 ostatnich miesięcy','form_dochody_6m'=>'dochód netto z 6 ostatnich miesięcy','form_dochody_12m'=>'dochód netto z 12 ostatnich miesięcy','form_waluta'=>'W jakiej walucie dochód','form_2_zrodlo'=>'Wynagrodzenie z drugiego źródła dochodu','form_2dochody'=>'Z jakiego tytułu są dochody','form_2dochody_inne'=>'Z jakiego tytułu są dochody','form_2dochody_od'=>'od kiedy','form_2dochody_do'=>'do kiedy','form_2dochody_ktora'=>'która to umowa z kolei','form_2dochody_3m'=>'dochód netto z 3 ostatnich miesięcy','form_2dochody_6m'=>'dochód netto z 6 ostatnich miesięcy','form_2dochody_12m'=>'dochód netto z 12 ostatnich miesięcy','form_2dochody_waluta'=>'W jakiej walucie dochód','form_utrzymanie_dorosle'=>'Ilość osób na utrzymaniu','form_utrzymanie_dzieci'=>'Ilość osób na utrzymaniu','form_kredyty'=>'Posiadane kredyty?','form_krd_rata'=>'Miesięczna rata (suma rat)','form_krd_pozostalo'=>'Pozostała kwota do spłaty','form_krd_zaleglosci'=>'Czy były zaległości w spłatach?','form_posiadanie_ror'=>'Posiadanie Rachunku Oszczędnościowo-Rozliczeniowego z przyznanym limitem kredytowym','form_posiadanie_ror_limit'=>'Przyznany limit','form_posiadanie_karty'=>'Posiadanie Karty Kredytowej','form_posiadanie_karty_limit'=>'Przyznany limit','form_posiadanie_samochodu'=>'Posiadane samochody','form_posiadane_rach_bankowe'=>'Posiadane rachunki bankowe (w jakich bankach)','form_drugi_kredytobiorca'=>'Drugi kredytobiorca','form_imie_2'=>'Imię','form_nazwisko_2'=>'Nazwisko','form_email_2'=>'E-mail','form_telefon_2'=>'Nr. Telefonu','form_cel_2'=>'Cel kredytu','form_miejscowosc_2'=>'Miejscowość','form_zabezpiecznie_2'=>'Wartość zabezpieczenia','form_wykonczenie_2'=>'Wykończenie','form_wykonczenie_kwota_2'=>'Kwota','form_kwota_kredytu_2'=>'Kwota kredytu','form_wklad_wlasny_2'=>'Posiadany wkład własny','form_metraz_2'=>'Metraż','form_sklad_2[]'=>'Co wchodzi w skład ceny nieruchomości','form_sklad_2[]'=>'Co wchodzi w skład ceny nieruchomości','form_sklad_2[]'=>'Co wchodzi w skład ceny nieruchomości','form_sklad_2[]'=>'Co wchodzi w skład ceny nieruchomości','form_sklad_2[]'=>'Co wchodzi w skład ceny nieruchomości','form_sklad_2'=>'Co wchodzi w skład ceny nieruchomości','form_sklad_inne_2'=>'Mieszkanie/ dom segment garaż komórka lokatorskamiejsce postojoweinne','form_rynek_2'=>'Rynek','form_data_urodzenia_2'=>'Data urodzenia kredytobiorcy','form_czdzialalnosc_2'=>'Czy jest prowadzona działalność gospodarcza','form_dzialalnosc_2'=>'Działalność','form_rycz_2015_mc_2'=>'Przychód za rok 2015 od stycznia do','form_rycz_2015_2'=>'Przychód za rok 2015 od stycznia do','form_rycz_2014_2'=>'Przychód za rok 2014','form_rycz_2013_2'=>'Przychód za rok 2013','form_zas_2015_mc_2'=>'Przychód z działalności za rok 2015 od stycznia do','form_zas_2015_2'=>'Przychód z działalności za rok 2015 od stycznia do','form_zas_2015_mc_koszty_2'=>'Koszty z działalności za rok 2015 od stycznia do','form_zas_2015_2'=>'Koszty z działalności za rok 2015 od stycznia do','form_zas_2014_2'=>'Przychód z działalności za rok 2014','form_zas_2014_koszty_2'=>'Koszty z działalności za rok 2014','form_zas_2013_2'=>'Przychód z działalności za rok 2013','form_zas_2013_koszty_2'=>'Koszty z działalności za rok 2013','form_zas_podatek_2'=>'Podatek','form_dochody_2'=>'Z jakiego tytułu są dochody','form_dochody_inne_2'=>'Z jakiego tytułu są dochody','form_dochody_od_2'=>'od kiedy','form_dochody_do_2'=>'do kiedy','form_dochody_ktora_2'=>'która to umowa z kolei','form_dochody_3m_2'=>'dochód netto z 3 ostatnich miesięcy','form_dochody_6m_2'=>'dochód netto z 6 ostatnich miesięcy','form_dochody_12m_2'=>'dochód netto z 12 ostatnich miesięcy','form_waluta_2'=>'W jakiej walucie dochód','form_2_zrodlo_2'=>'Wynagrodzenie z drugiego źródła dochodu','form_2dochody_2'=>'Z jakiego tytułu są dochody','form_2dochody_inne_2'=>'Z jakiego tytułu są dochody','form_2dochody_od_2'=>'od kiedy','form_2dochody_do_2'=>'do kiedy','form_2dochody_ktora_2'=>'która to umowa z kolei','form_2dochody_3m_2'=>'dochód netto z 3 ostatnich miesięcy','form_2dochody_6m_2'=>'dochód netto z 6 ostatnich miesięcy','form_2dochody_12m_2'=>'dochód netto z 12 ostatnich miesięcy','form_2dochody_waluta_2'=>'W jakiej walucie dochód','form_utrzymanie_dorosle_2'=>'Ilość osób na utrzymaniu','form_utrzymanie_dzieci_2'=>'Ilość osób na utrzymaniu','form_kredyty_2'=>'Posiadane kredyty?','form_krd_rata_2'=>'Miesięczna rata (suma rat)','form_krd_pozostalo_2'=>'Pozostała kwota do spłaty','form_krd_zaleglosci_2'=>'Czy były zaległości w spłatach?','form_posiadanie_ror_2'=>'Posiadanie Rachunku Oszczędnościowo-Rozliczeniowego z przyznanym limitem kredytowym','form_posiadanie_ror_limit_2'=>'Przyznany limit','form_posiadanie_karty_2'=>'Posiadanie Karty Kredytowej','form_posiadanie_karty_limit_2'=>'Przyznany limit','form_posiadanie_samochodu_2'=>'Posiadane samochody','form_posiadane_rach_bankowe_2'=>'Posiadane rachunki bankowe (w jakich bankach)');

$mail_from = (isset($_POST['email']))?$_POST['email']:$_POST['form_email'];
$mail_from_name = (isset($_POST['nazwisko']))?$_POST['nazwisko']:$_POST['form_imie'].' '.$_POST['form_nazwisko'];
$_head = '';
$kr = false;
if($_POST['form']=="kredyt")
{
	$_head = " Kredyt ";
	$kr = true;
unset($_POST['form_akceptuje']);
unset($_POST['form_zgoda']);
unset($_POST['time']);
}
else
{
	$_head = " Kontakt ";
}
unset($_POST['form']);
$mail_content = '';
	if($kr)
		if(isset($_POST['form_drugi_kredytobiorca']))
			if($_POST['form_drugi_kredytobiorca']=="Tak")
				$mail_content .= "\n\rPIERWSZY KREDYTOBIORCA\n\r";
foreach($_POST as $field => $val)
{
	if(empty($val)) 
		continue;
	if(is_array($val))
	{
		if($kr)
			$field = $name_kr[$field];
		$mail_content .= $field.': ';
		foreach($val as $v)
		{
			$mail_content .= $v .'; ';
		}
			$mail_content .= "\n\r";
	}
	else
	{
		if($field == 'form_drugi_kredytobiorca' && $val=="Tak")
			$mail_content .= "\n\rDRUGI KREDYTOBIORCA\n\r";
		if($kr)
			$field = $name_kr[$field];
		$mail_content .= $field.': '.$val."\n\r";
	}
}
$to      = 'biuro@negocjatornia.pl';
$subject = "Negocjatornia$_head- ".$mail_from_name;
$headers = 'From: '.$mail_from . "\r\n" .
    'Reply-To: '.$mail_from . "\r\n" .
   "MIME-Version: 1.0" . "\r\n" .
   "Content-type: text/plain; charset=UTF-8" . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$message = $mail_content;
		if(@mail($to, $subject, $message, $headers)) {
		  	$info = 'Dziękujemy,  wkrótce się odezwiemy.';
		}
		else {
					$info = 'Wystąpił błąd, spróbuj ponownie.';
		}
		echo $info;
		file_put_contents('mail.txt',$to."\n".$subject."\n".$headers."\n\n".$mail_content);
